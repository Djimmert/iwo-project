# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository is for working on my research project, I'm doing for the course IWO.
I'm following this course at the University of Groningen (RUG).
This is version 0.01 of the README-file

### What is this research about? ###

The research I'm conducting is about how the use of certain English words have increased
in use in Dutch Tweets, over the last few years.
I'm using the in-house twitter-data of the RUG for this.