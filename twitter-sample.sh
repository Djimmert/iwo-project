#!/bin/bash

# Creating temporary files
TWEETAMOUNT='mktemp'
UTWEETS='mktemp'
UTEXTTWEETS='mktemp'


zless /net/corpora/twitter2/Tweets/2017/03/20170301\:11.out.gz | /net/corpora/twitter2/tools/tweet2tab -i id | wc -l > $TWEETAMOUNT
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:11.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort -u | wc -l > $UTWEETS
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:11.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | $UTEXTTWEETS


awk '!x[$UTEXTTWEETS]++'

# Removing temporary files
rm -f $TWEETAMOUNT
rm -f $UTWEETS
rm -f $UTEXTTWEETS